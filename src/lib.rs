mod contract_api;

use rand::SeedableRng;
use rand_chacha::ChaCha20Rng;

pub fn thread_rng() -> ChaCha20Rng {
  let seed: [u8; 32] = {
    let bytes = contract_api::random_bytes();
    let mut seed = [0u8; 32];
    seed.copy_from_slice(&bytes);
    seed
  };
  ChaCha20Rng::from_seed(seed)
}
