extern "C" {
  pub fn casper_revert(status: u32) -> !;
  pub fn casper_random_bytes(out_ptr: *mut u8, out_size: usize) -> i32;
}

#[derive(Copy, Clone, PartialEq, Eq)]
#[non_exhaustive]
pub enum ApiError {
  Unhandled = 31,
}

pub fn revert(error: ApiError) -> ! {
  unsafe {
    casper_revert(error as u32);
  }
}

/// Number of random bytes returned from the `random_bytes()` function.
const RANDOM_BYTES_COUNT: usize = 32;

/// Returns 32 pseudo random bytes.
pub fn random_bytes() -> [u8; RANDOM_BYTES_COUNT] {
  let mut ret = [0; RANDOM_BYTES_COUNT];
  let result = unsafe { casper_random_bytes(ret.as_mut_ptr(), RANDOM_BYTES_COUNT) };
  if result != 0 {
    revert(ApiError::Unhandled);
  }
  ret
}
